## Why?
- Searches Craigslist for information
- Saves times
- Minimal user interface.

## Requirements
- Docker, docker-compose

## Installation
In the root directory: "docker-compose up".
This will fetch all files.

## Use
In your web browser of choice navigate to:

- localhost/findjob
- localhost/findjoblocal
- localhost/findcarlocal

## Warning
- This was one of my first PHP projects ever.  
- It had never been updated to best practices.  
- The code quality is rather terrible.  

## SSH into a Container

    Use docker ps to get the name of the existing container.
    Use the command docker exec -it <container name> /bin/bash to get a bash shell in the container.
    Generically, use docker exec -it <container name> <command> to execute whatever command you specify in the container.

## Stop and remove all docker containers and images

    List all containers (only IDs) docker ps -aq.
    Stop all running containers. docker stop $(docker ps -aq)
    Remove all containers. docker rm $(docker ps -aq)
    Remove all images. docker rmi -f $(docker images -q)
