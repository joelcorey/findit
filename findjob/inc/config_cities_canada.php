<?php

return array(
  array(
		'Alberta',
		'https://calgary.craigslist.ca/',
		'https://edmonton.craigslist.ca/',
		'https://ftmcmurray.craigslist.ca/',
		'https://lethbridge.craigslist.ca/',
		'https://hat.craigslist.ca/',
		'https://peace.craigslist.ca/',
		'https://reddeer.craigslist.ca/'
    ),
    array(
		'British Columbia',
		'https://cariboo.craigslist.ca/',
		'https://comoxvalley.craigslist.ca/',
		'https://abbotsford.craigslist.ca/',
		'https://kamloops.craigslist.ca/',
		'https://kelowna.craigslist.ca/',
		'https://kootenays.craigslist.ca/',
		'https://nanaimo.craigslist.ca/',
		'https://princegeorge.craigslist.ca/',
		'https://skeena.craigslist.ca/',
		'https://sunshine.craigslist.ca/',
		'https://vancouver.craigslist.ca/',
		'https://victoria.craigslist.ca/',
		'https://whistler.craigslist.ca/'
    ),
    array(
		'Manitoba',
		'https://winnipeg.craigslist.ca/',
	),  
	array(
		'New Brunswick',
		'https://newbrunswick.craigslist.ca/',
	),  
	array(
		'Newfoundland and Labrador',
		'https://newfoundland.craigslist.ca/',
	),  
	array(
		'Northwest Territories',
		'https://territories.craigslist.ca/',
		'https://yellowknife.craigslist.ca/'
	),  
	array(
		'Nova Scotia',
		'https://halifax.craigslist.ca/',
	),  
	array(
		'Ontario',
		'https://barrie.craigslist.ca/',
		'https://belleville.craigslist.ca/',
		'https://brantford.craigslist.ca/',
		'https://chatham.craigslist.ca/',
		'https://cornwall.craigslist.ca/',
		'https://guelph.craigslist.ca/',
		'https://hamilton.craigslist.ca/',
		'https://kingston.craigslist.ca/',
		'https://kitchener.craigslist.ca/',
		'https://londonon.craigslist.ca/',
		'https://niagara.craigslist.ca/',
		'https://ottawa.craigslist.ca/',
		'https://owensound.craigslist.ca/',
		'https://peterborough.craigslist.ca/',
		'https://sarnia.craigslist.ca/',
		'https://soo.craigslist.ca/',
		'https://sudbury.craigslist.ca/',
		'https://thunderbay.craigslist.ca/',
		'https://toronto.craigslist.ca/',
		'https://windsor.craigslist.ca/',
	),  
	array(
		'Prince Edward Island',
		'https://pei.craigslist.ca/',
	),
	array(
		'Quebec',
		'https://montreal.craigslist.ca/',
		'https://quebec.craigslist.ca/',
		'https://saguenay.craigslist.ca/',
		'https://sherbrooke.craigslist.ca/',
		'https://troisrivieres.craigslist.ca/',
	),  
	array(
		'Saskatchewan',
		'https://regina.craigslist.ca/',
		'https://saskatoon.craigslist.ca/',
	),  
	array(
		'Yukon Territory',
		'https://whitehorse.craigslist.ca/',
	),  
);