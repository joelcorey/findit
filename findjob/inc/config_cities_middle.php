<?php

return array(
    // array(
    //     'Illinois',
    //     'bn.craigslist.org/',
    //     'chambana.craigslist.org/',
    //     'chicago.craigslist.org/',
    //     'decatur.craigslist.org/',
    //     'lasalle.craigslist.org/',
    //     'mattoon.craigslist.org/',
    //     'peoria.craigslist.org/',
    //     'rockford.craigslist.org/',
    //     'carbondale.craigslist.org/',
    //     'springfieldil.craigslist.org/',
    //     'quincy.craigslist.org/',
    // ),
    array(
        'Iowa',
        'ames.craigslist.org/',
        'cedarrapids.craigslist.org/',
        'desmoines.craigslist.org/',
        'dubuque.craigslist.org/',
        'fortdodge.craigslist.org/',
        'iowacity.craigslist.org/',
        'masoncity.craigslist.org/',
        'quadcities.craigslist.org/',
        'siouxcity.craigslist.org/',
        'ottumwa.craigslist.org/',
        'waterloo.craigslist.org/',
    ),
    array(
        'Kansas',
        'lawrence.craigslist.org/',
        'ksu.craigslist.org/',
        'nwks.craigslist.org/',
        'salina.craigslist.org/',
        'seks.craigslist.org/',
        'swks.craigslist.org/',
        'topeka.craigslist.org/',
        'wichita.craigslist.org/',
    ),
    array(
        'Missouri',
        'columbiamo.craigslist.org/',
        'joplin.craigslist.org/',
        'kansascity.craigslist.org/',
        'kirksville.craigslist.org/',
        'loz.craigslist.org/',
        'semo.craigslist.org/',
        'springfield.craigslist.org/',
        'stjoseph.craigslist.org/',
        'stlouis.craigslist.org/',
    ),
    array(
        'Nebraska',
        'grandisland.craigslist.org/',
        'lincoln.craigslist.org/',
        'northplatte.craigslist.org/',
        'omaha.craigslist.org/',
        'scottsbluff.craigslist.org/',
    ),
    array(
        'North Dakota',
        'bismarck.craigslist.org/',
        'fargo.craigslist.org/',
        'grandforks.craigslist.org/',
        'nd.craigslist.org/',
    ),
    array(
        'South Dakota',
        'nesd.craigslist.org/',
        'csd.craigslist.org/',
        'rapidcity.craigslist.org/',
        'siouxfalls.craigslist.org/',
        'sd.craigslist.org/',
    ),
);