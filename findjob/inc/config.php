<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';

$cities = require_once 'config_cities_area.php';
// $api_keys = require_once 'config_api_keys.php';
require_once 'useragentlist.php';

// $apiKey = $api_keys[0];
// $secretKey = $api_keys[1];

$dayIntoPast = 2;

$categories = array(
  // 'd/internet-engineering/search/eng', // internet engineering
  // 'd/software-qa-dba-etc/search/sof', // software/qa/dba/etc
  // 'd/systems-networking/search/sad', // systems/networking
  'd/web-html-info-design/search/web', // web/html/info design
  'd/computer-gigs/search/cpg' // computer gigs
  );

$keywords = array(
  "angular",
  "api",
  "associate",
  "automation",
  "bash",
  "bolt",
  "C#",
  "cms",
  "gis",
  "html",
  "javascript",
  "junior",
  "lemp",
  "linux",
  ".net",
  "nginx",
  "php",
  "pwa",
  "remote",
  "rust",
  "script",
  "shopify",
  "telecommute",
  "ubuntu",
  "vue",
  "web",
  "wordpress"
);

// get rid of unwanteds
$filterExact = array(
  'FT/PT - LEGIT WORK FROM HOME - FREE WEBSITE',
  'FT/PT - Legit WORK FROM HOME - FREE WEBSITE',
  'Professor of Back-End App and Web Development',
  'Become An Active Associate Today - For Free Advertising In Any City',
  'Now hiring Online promoters and associates!',
  '***REMOTE*** White Hat Hacking Expert Needed as InfoSec HowTo Writer',
  'Earn Money While You Sleep Selling CBD Oil Online - FREE WEBSITE',
  'Simple, copy/paste your free websites CTFO CBD products',
  'looking for a female student for web',
  '2 Free Websites, start your business now! Cut,Copy Paste',
);

$filterContains = array(
  'In House Only',
  'LAS VEGAS LOCAL ONLY',
  'Remote Sales Operatives Needed',
  'Per Month Unlimited Revisions & No Upfront Costs!',
  'In-House',
  'Christian',
  'IN PERSON',
  'LAS VEGAS LOCAL ONLY',
);
