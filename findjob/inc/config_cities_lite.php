<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'useragentlist.php';


$categories = array(
  'd/internet-engineering/search/eng', // internet engineering
  'd/software-qa-dba-etc/search/sof', // software/qa/dba/etc
  'd/systems-networking/search/sad', // systems/networking
  'd/web-html-info-design/search/web', // web/html/info design
  'd/computer-gigs/search/cpg' // computer gigs
  );

$keywords = array(
  "angular",
  "api",
  "associate",
  "automation",
  "bash",
  "bolt",
  "C#",
  "cms",
  "gis",
  "html",
  "javascript",
  "junior",
  "lemp",
  "linux",
  ".net",
  "nginx",
  "php",
  "pwa",
  "remote",
  "rust",
  "script",
  "telecommute",
  "ubuntu",
  "vue",
  "web",
  "wordpress"
);

$master = array(
    // array(
    // 'Alabama',
    // 'auburn.craigslist.org/',
    // 'bham.craigslist.org/',
    // 'dothan.craigslist.org/',
    // 'shoals.craigslist.org/',
    // 'gadsden.craigslist.org/',
    // 'huntsville.craigslist.org/',
    // 'mobile.craigslist.org/',
    // 'montgomery.craigslist.org/',
    // 'tuscaloosa.craigslist.org/'
    // ),
    // array(
    // 'Alaska',
    // 'anchorage.craigslist.org/',
    // 'fairbanks.craigslist.org/',
    // 'kenai.craigslist.org/',
    // 'juneau.craigslist.org/'
    // ),
    // array(
    // 'Arizona',
    // 'flagstaff.craigslist.org/',
    // 'mohave.craigslist.org/',
    // 'phoenix.craigslist.org/',
    // 'prescott.craigslist.org/',
    // 'showlow.craigslist.org/',
    // 'sierravista.craigslist.org/',
    // 'tucson.craigslist.org/',
    // 'yuma.craigslist.org/'
    // ),
    // array(
    // 'Arkansas',
    // 'bakersfield.craigslist.org/',
    // 'chico.craigslist.org/',
    // 'fresno.craigslist.org/',
    // 'goldcountry.craigslist.org/',
    // 'hanford.craigslist.org/',
    // 'humboldt.craigslist.org/',
    // 'imperial.craigslist.org/',
    // 'inlandempire.craigslist.org/',
    // 'losangeles.craigslist.org/',
    // 'mendocino.craigslist.org/',
    // 'merced.craigslist.org/',
    // 'modesto.craigslist.org/',
    // 'monterey.craigslist.org/',
    // 'orangecounty.craigslist.org/',
    // 'palmsprings.craigslist.org/',
    // 'redding.craigslist.org/',
    // 'sacramento.craigslist.org/',
    // 'sandiego.craigslist.org/',
    // 'sfbay.craigslist.org/',
    // 'slo.craigslist.org/',
    // 'santabarbara.craigslist.org/',
    // 'santamaria.craigslist.org/',
    // 'siskiyou.craigslist.org/',
    // 'stockton.craigslist.org/',
    // 'susanville.craigslist.org/',
    // 'ventura.craigslist.org/',
    // 'visalia.craigslist.org/',
    // 'yubasutter.craigslist.org/'
    // ),
    // array(
    // 'Colorado',
    // 'boulder.craigslist.org/',
    // 'cosprings.craigslist.org/',
    // 'denver.craigslist.org/',
    // 'eastco.craigslist.org/',
    // 'fortcollins.craigslist.org/',
    // 'rockies.craigslist.org/',
    // 'pueblo.craigslist.org/',
    // 'westslope.craigslist.org/'
    // ),
    // array(
    // 'Hawaii',
    // 'honolulu.craigslist.org/'
    // ),
    // array(
    // 'Idaho',
    // 'boise.craigslist.org/',
    // 'eastidaho.craigslist.org/',
    // 'lewiston.craigslist.org/',
    // 'twinfalls.craigslist.org/'
    // ), 
    // array(
    // 'Iowa',
    // 'ames.craigslist.org/',
    // 'cedarrapids.craigslist.org/',
    // 'desmoines.craigslist.org/',
    // 'dubuque.craigslist.org/',
    // 'fortdodge.craigslist.org/',
    // 'iowacity.craigslist.org/',
    // 'masoncity.craigslist.org/',
    // 'quadcities.craigslist.org/',
    // 'siouxcity.craigslist.org/',
    // 'ottumwa.craigslist.org/',
    // 'waterloo.craigslist.org/'
    // ),
    // array(
    // 'Kansas',
    // 'lawrence.craigslist.org/',
    // 'ksu.craigslist.org/',
    // 'nwks.craigslist.org/',
    // 'salina.craigslist.org/',
    // 'seks.craigslist.org/',
    // 'swks.craigslist.org/',
    // 'topeka.craigslist.org/',
    // 'wichita.craigslist.org/'
    // ),
    // array(
    // 'Minnesota',
    // 'bemidji.craigslist.org/',
    // 'brainerd.craigslist.org/',
    // 'duluth.craigslist.org/',
    // 'mankato.craigslist.org/',
    // 'minneapolis.craigslist.org/',
    // 'rmn.craigslist.org/',
    // 'marshall.craigslist.org/',
    // 'stcloud.craigslist.org/'
    // ),
    // array(
    // 'Montana',
    // 'billings.craigslist.org/',
    // 'bozeman.craigslist.org/',
    // 'butte.craigslist.org/',
    // 'greatfalls.craigslist.org/',
    // 'helena.craigslist.org/',
    // 'kalispell.craigslist.org/',
    // 'missoula.craigslist.org/',
    // 'montana.craigslist.org/'
    // ),  
    // array(
    // 'Nebraska',
    // 'grandisland.craigslist.org/',
    // 'lincoln.craigslist.org/',
    // 'northplatte.craigslist.org/',
    // 'omaha.craigslist.org/',
    // 'scottsbluff.craigslist.org/'
    // ),
    // array(
    // 'Nevada',
    // 'elko.craigslist.org/',
    // 'lasvegas.craigslist.org/',
    // 'reno.craigslist.org/'
    // ),

    // array(
    // 'New Mexico',
    // 'albuquerque.craigslist.org/',
    // 'clovis.craigslist.org/',
    // 'farmington.craigslist.org/',
    // 'lascruces.craigslist.org/',
    // 'roswell.craigslist.org/',
    // 'santafe.craigslist.org/'
    // ),


    // array(
    // 'North Dakota',
    // 'bismarck.craigslist.org/',
    // 'fargo.craigslist.org/',
    // 'grandforks.craigslist.org/',
    // 'nd.craigslist.org/'
    // ),

    // array(
    // 'Oklahoma',
    // 'lawton.craigslist.org/',
    // 'enid.craigslist.org/',
    // 'oklahomacity.craigslist.org/',
    // 'stillwater.craigslist.org/',
    // 'tulsa.craigslist.org/'
    // ),
    // array(
    // 'South Dakota',
    // 'nesd.craigslist.org/',
    // 'csd.craigslist.org/',
    // 'rapidcity.craigslist.org/',
    // 'siouxfalls.craigslist.org/',
    // 'sd.craigslist.org/'
    // ),
    // array(
    // 'Texas',
    // 'abilene.craigslist.org/',
    // 'amarillo.craigslist.org/',
    // 'austin.craigslist.org/',
    // 'beaumont.craigslist.org/',
    // 'brownsville.craigslist.org/',
    // 'collegestation.craigslist.org/',
    // 'corpuschristi.craigslist.org/',
    // 'dallas.craigslist.org/',
    // 'nacogdoches.craigslist.org/',
    // 'delrio.craigslist.org/',
    // 'elpaso.craigslist.org/',
    // 'galveston.craigslist.org/',
    // 'houston.craigslist.org/',
    // 'killeen.craigslist.org/',
    // 'laredo.craigslist.org/',
    // 'lubbock.craigslist.org/',
    // 'mcallen.craigslist.org/',
    // 'odessa.craigslist.org/',
    // 'sanangelo.craigslist.org/',
    // 'sanantonio.craigslist.org/',
    // 'sanmarcos.craigslist.org/',
    // 'bigbend.craigslist.org/',
    // 'texoma.craigslist.org/',
    // 'easttexas.craigslist.org/',
    // 'victoriatx.craigslist.org/',
    // 'waco.craigslist.org/',
    // 'wichitafalls.craigslist.org/'
    // ),
    // array(
    // 'Utah',
    // 'logan.craigslist.org/',
    // 'ogden.craigslist.org/',
    // 'provo.craigslist.org/',
    // 'saltlakecity.craigslist.org/',
    // 'stgeorge.craigslist.org/'
    // ),
    // array(
    // 'Wisconsin',
    // 'appleton.craigslist.org/',
    // 'eauclaire.craigslist.org/',
    // 'greenbay.craigslist.org/',
    // 'janesville.craigslist.org/',
    // 'racine.craigslist.org/',
    // 'lacrosse.craigslist.org/',
    // 'madison.craigslist.org/',
    // 'milwaukee.craigslist.org/',
    // 'northernwi.craigslist.org/',
    // 'sheboygan.craigslist.org/',
    // 'wausau.craigslist.org/'
    // ),
    // array(
    // 'Wyoming',
    // 'wyoming.craigslist.org/'
    // ),
);