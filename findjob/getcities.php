<!doctype html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>jobMaker</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="js/jquery-3.3.1.min.js"></script>

</head>
<body>

<?php

function crawley($url, $agent) {

	$ch = curl_init();

	$options = array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_FOLLOWLOCATION => TRUE,
		CURLOPT_AUTOREFERER => TRUE,
		CURLOPT_USERAGENT => $agent,
		CURLOPT_TIMEOUT => 1800
	);

	curl_setopt_array($ch, $options);

	$data = curl_exec($ch);

	if(!$data){
		echo "<br />cURL error number: " . curl_errno($ch);
		echo "<br />cURL error: " . curl_error($ch) . " on URL - " . $url;
		var_dump(curl_getinfo($ch));
		var_dump(curl_error($ch));
		exit;
	}

	curl_close($ch);

	return $data;
}

function scrape_between($data, $start, $end){
  $data = stristr($data, $start); // Stripping all data from before $start
  $data = substr($data, strlen($start));  // Stripping $start
  $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
  $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
  return $data;   // Returning the scraped data from the function
}

function match_keywords(/*array &*/$search, /*array &*/$keys) { 	// A function to replace the foreach keyword search
	$datamatch = NULL;
	foreach ($keys as $key) {	
		if (stripos($search, $key) !== false) {
			$datamatch = $search;
		}
	}
return $datamatch;
}

$url = 'https://www.craigslist.org/about/sites';
$responsePage = crawley($url, $useragent);


$colMask = explode('<div class="colmask"', $responsePage);
$column = explode('<div class="box box_1"', $colMask[1]);
$byState = explode('<h4', $column[1]);

//echo $byState[1];

foreach ($byState as $state) 
{
    $stateName = scrape_between($state, '>', '</h4>');
    //echo $stateName . '</br>';

    $cities = explode('<li', $state);
    print_r($cities);
    foreach($cities as $city)
    {
        echo $city[0];
        $href = scrape_between($city,'><a href="https://"' ,'"');
        //echo $href . '</br>';
    }
    
}

//echo $byState[0];
// foreach ($column as $col) {
//     echo $col;
// }


die();


?>

<script>

</script>

</ul>
</html>